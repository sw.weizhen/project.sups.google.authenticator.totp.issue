FROM ubuntu:latest

RUN mkdir /project.sups.google.authenticator.totp.issue

WORKDIR /project.sups.google.authenticator.totp.issue

COPY . .

CMD ["./totpserv"]