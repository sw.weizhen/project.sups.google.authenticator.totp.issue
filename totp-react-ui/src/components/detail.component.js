import React, { Component } from "react";
import './detail.component.css';
import './config.addr.js'
import { totpserv_address } from "./config.addr.js";

let usersData= []

export default class UserDetail extends Component {

    constructor() {
        super()
        this.state = { 
           supersetUsers: usersData
        };
    }
   
    componentDidMount(){
        this.fetchData();
    }
   
    fetchData() {
        fetch( totpserv_address + '/totp/users',{method:"GET"})
        .then(res => res.json())
        .then(data => {
            if (data['success']) {
                this.setState({supersetUsers: data['data']});
            } else {
                window.location.replace("/sign-in");
            }

        })
        .catch(e => {
            alert(e);
            window.location.replace("/sign-in");
        })
    }
    

    renderTableData() {
        return this.state.supersetUsers.map((supersetUsers) => {
           const { username, secret } = supersetUsers
           return (
               
                <tr key = {username}>
                    <td>{username}</td>
                    <td>{secret}</td>
                    <td>
                        <button className="qrBtn" onClick={this.handleChgPassword} id = {username}>Password</button>&emsp;
                        <button className="qrBtn" onClick={this.handleQRCodeDownload} id = {username}>QRCode</button>
                    </td>
                </tr>
           )
        })
    }

    handleChgPassword = (e) => {
        let account = e.currentTarget.id
        document.getElementById("myModal").style.display = "block";
        document.getElementById("txtAcnt").value = account;
        document.getElementById("txtAcnt").disabled = true;

        document.getElementById("errTip").innerHTML = "";
    }

    handleQRCodeDownload = (e) => {

        let filename = e.currentTarget.id + ".png";
        let path = totpserv_address + '/totp/qrcode/'+ e.currentTarget.id;
        
        fetch( path, {method:"GET"})
        .then(res => res.json())
        .then(data => {
            if (data['success']) {
                const linkSource = data['data'];
                const downloadLink = document.createElement("a");
                downloadLink.href = linkSource;
                downloadLink.download = filename;
                downloadLink.click();

            } else {
                console.log(data['errMsg']);
                window.location.replace("/sign-in");
            }

        })
        .catch(e => {
            console.log(e);
            window.location.replace("/sign-in");
        })
     }
   

    handleClosePass = (e) => {
        document.getElementById("myModal").style.display = "none";
    }
    
    handleSubmitPass = (event) => {
        event.preventDefault();

        const account = event.target.account.value;
        const new_pass = event.target.newpassword.value;
        const con_pass = event.target.confirmpassword.value;
        
        fetch( totpserv_address + '/totp/superset/pass', {
            method: "POST",
            body: new URLSearchParams({
                'account': account,
                'password': new_pass,
                'confirmpassword': con_pass
            })            
        })
        .then(res => res.json())
        .then(data => {
            if (data['success']) {
                console.log("change password ok")
                document.getElementById("myModal").style.display = "none";
                document.getElementById("errTip").innerHTML = "";

            } else {
                document.getElementById("errTip").innerHTML = '<p id="loginTips">' + data['errMsg'] + '</p>';

            }
        })
        .catch(e => {
            alert(e);

            document.getElementById("myModal").style.display = "none";
        })
    }

    render() {
        return (
            <div>
                <table className = 'qrTBL' id='tblTOTPUserList'>
                    <tbody>
                        <tr>
                            <th>TOTP Account</th>
                            <th>Secret</th>
                            <th>Operate</th>
                        </tr>
                            {this.renderTableData()}
                    </tbody>
                </table>
                <div id="myModal" className="modal">
                    <div className="modal-content">
                        <form className = "modal-form" onSubmit={this.handleSubmitPass}>
                            <h3>TOTP User Password</h3>
                            <div className="form-group">
                                <label>Account</label>
                                <input type="text" name="account" className="form-control" id="txtAcnt"/>
                            </div>
                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" name="newpassword" className="form-control" placeholder="Enter new password" />
                            </div>
                            <div className="form-group">
                                <label>Confirm password</label>
                                <input type="password" name="confirmpassword" className="form-control" placeholder="Confirm new password" />
                            </div>
                            <div className="form-group" id="errTip">
                            </div>
                            <button id = "cbta" type="submit" className="btn btn-primary btn-block">Confirm</button>
                            <button id = "cbtb" className="btn btn-primary btn-block" onClick={this.handleClosePass}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
