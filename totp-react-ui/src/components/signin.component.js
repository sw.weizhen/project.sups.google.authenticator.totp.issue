import React, { Component } from "react";
import './signin.component.css';
import { totpserv_address } from "./config.addr.js";


export default class SingIn extends Component {

    constructor(props) {
        super(props);

        // this.state = {
        //     errMsg: ""
        // }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const account = event.target.account.value;
        const password = event.target.password.value;
        
        fetch( totpserv_address + '/totp/superset/login', {
            method: "POST",
            body: new URLSearchParams({
                'account': account,
                'password': password,
            })            
        })
        .then(res => res.json())
        .then(data => {
            console.log(data['success'])
            if (data['success']) {
                let innerhtml = '<img id="qrcid" className="pngCont" src="' + data['data'] + '"></img>'
                document.getElementById("msgTip").innerHTML = innerhtml;
            } else {
                // this.setState(
                //     {errMsg: data['errMsg']}
                // )
                let innerhtml = '<p id="loginTips">' + data['errMsg'] + '</p>';
                var ell = document.getElementById("msgTip");
                ell.innerHTML = innerhtml;
            }
        })
        .catch(e => {
            alert(e);
        })
    }
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h3>TOTP User</h3>
                <div className="form-group">
                    <label>Account</label>
                    <input type="text" name="account" className="form-control" placeholder="Enter account" />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" placeholder="Enter password" />
                </div>
                <div className="form-group" id="msgTip">
                    {/* <p id="loginTips"></p> */}
                </div>
                {/* <div className="qrcode-display">
                   <img id="qrcid" className="pngCont" src=""></img>
                </div> */}
                <button type="submit" className="btn btn-primary btn-block">Query QRCode</button>
            </form>
            
        );
    }
}
