import React, { Component } from "react";
import './poweruser_signin.component.css';
import { totpserv_address } from "./config.addr.js";


export default class PowerUserSingIn extends Component {

    constructor(props) {
        super(props);

        // this.state = {
        //     errMsg: ""
        // }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const account = event.target.account.value;
        const password = event.target.password.value;
        
        fetch( totpserv_address + '/totp/power/login', {
            method: "POST",
            body: new URLSearchParams({
                'account': account,
                'password': password,
            })            
        })
        .then(res => res.json())
        .then(data => {
            if (data['success']) {
                document.getElementById("errTip").innerHTML = "";
                window.location.replace("/user-detail");
                
            } else {
                // this.setState(
                //     {errMsg: data['errMsg']}
                // )
                let innerhtml = '<p id="loginTips">' + data['errMsg'] + '</p>';
                var el = document.getElementById("errTip");
                el.innerHTML = innerhtml;
            }
        })
        .catch(e => {
            alert(e);
        })
    }
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h3>Power User</h3>
                <div className="form-group">
                    <label>Account</label>
                    <input type="text" name="account" className="form-control" placeholder="Enter account" />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" placeholder="Enter password" />
                </div>
                <div className="form-group" id="errTip">
                    {/* <p id="loginTips"></p> */}
                </div>
                <button type="submit" className="btn btn-primary btn-block">Sign in</button>
            </form>
            
        );
    }
}
